# SnowShu.org 
_a simple static website in Go._

## Developing
To run the hugo server with hot reload:

```
docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo jguyomard/hugo-builder hugo server -w --bind=0.0.0.0 
```

### Compiling CSS

```
docker run -it --entrypoint '' -v ${PWD}:/src antonienko/compass-watch sass --watch static/scss:static/css
```